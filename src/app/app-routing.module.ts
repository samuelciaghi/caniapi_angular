import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisualizzaCaniComponent } from './componenti/visualizza-cani/visualizza-cani.component';

const routes: Routes = [
  {path: '',component:VisualizzaCaniComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
