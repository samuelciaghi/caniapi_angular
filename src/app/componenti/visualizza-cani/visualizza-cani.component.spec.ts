import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaCaniComponent } from './visualizza-cani.component';

describe('VisualizzaCaniComponent', () => {
  let component: VisualizzaCaniComponent;
  let fixture: ComponentFixture<VisualizzaCaniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizzaCaniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaCaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
