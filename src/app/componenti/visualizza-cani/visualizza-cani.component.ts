import { Component, OnInit } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { GetCaniApiService } from 'src/app/services/get-cani-api.service';

@Component({
  selector: 'app-visualizza-cani',
  templateUrl: './visualizza-cani.component.html',
  styleUrls: ['./visualizza-cani.component.css']
})
export class VisualizzaCaniComponent implements OnInit {

  filtra: boolean = false;
  datiCaniRicevuti: any = [];
  JSONcompleto: any = [];
  JSON_CANI_FINALE: any = [];
  numeroCani: any;
  cercaCane:any;

  constructor(private caniService: GetCaniApiService) { }

  ngOnInit(): void {

    this.numeroCani = 'TUTTI';

    this.getCaniByBreed()
      .then(() => this.getFotoCani())
      .then((datiFinali) => console.log(datiFinali))

  }

  getCaniByBreed() {
    const listaCani = new Promise((resolve, reject) => {

      this.caniService.getCani().subscribe(cane => {
        this.datiCaniRicevuti = cane;
        resolve(this.datiCaniRicevuti);

      });
    })

    return listaCani
  }

  getFotoCani() {

    //Azzera l'array in caso di refresh
    this.JSONcompleto = [];
    let razze = [...this.datiCaniRicevuti.message];

    /* Dato che l'api resituisce solo un'immagine random in base alla 
     razza selezionata, creo un ciclo che fa il fetch dell'immagine per tutte le razze
     di cani che l'api possiede, in questo caso 94 */

    for (let i = 0; i < razze.length; i++) {

      let razza = razze[i];
      //fetch immagine cane random 
      this.caniService.getFotoCani(razza).subscribe((fotoCane: any) => {

        let tmp = { id: i + 1, 'img': fotoCane.message, 'nome': razze[i] };
        this.JSONcompleto.push(tmp);

      });
    }
    /*  Riordina i cani in base al loro id
     this.JSON_CANI_FINALE = this.JSONcompleto.sort((a, b) => { return a.id - b.id }); */

    this.JSON_CANI_FINALE = this.JSONcompleto;

    return this.JSONcompleto
  }

  riduciNumCani(numero) {

    this.numeroCani = numero;
    let listaFiltrabile = this.JSONcompleto;
    this.JSON_CANI_FINALE = [...listaFiltrabile].slice(0, numero);

    //Se si selezionano più di 50 immagini, vengono mostrate tutte quelle disponibili
    if (numero > 50) {
      this.JSON_CANI_FINALE = [...listaFiltrabile];
    }

    return this.JSON_CANI_FINALE
  }

}
