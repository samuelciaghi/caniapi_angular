import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Cani } from '../models/cani';

@Injectable({
  providedIn: 'root'
})
export class GetCaniApiService {

  constructor(private http:HttpClient) { }

  getCani(){
    return this.http.get('https://dog.ceo/api/breeds/list')
  }

  getFotoCani(razza){
    return this.http.get('https://dog.ceo/api/breed/'+razza+'/images/random')
  }
  
}
