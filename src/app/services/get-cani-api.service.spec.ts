import { TestBed } from '@angular/core/testing';

import { GetCaniApiService } from './get-cani-api.service';

describe('GetCaniApiService', () => {
  let service: GetCaniApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetCaniApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
